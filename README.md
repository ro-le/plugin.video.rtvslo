# plugin.video.rtvslo

=============

EN

DESCRIPTION: audio/video plugin for Kodi. It plays live streams and archived content made by RTV Slovenia.

SETUP: download release file (plugin.video.rtvslo-x.x.x.zip), then install plugin via Kodi interface.

IMPORTANT:
This repository hosts plugin version 3.x.x which breaks backwards compatibility and only supports Kodi 19 (Matrix)!
Devices running Kodi 18 (Leia) can still use older versions of this plugin (2.x.x) available at following github repository: https://github.com/ro-le/plugin.video.rtvslo.
However, version 2.1.3 is final version of the old plugin and will not be maintained in the future.

AUTHOR: ro-le

=============

SI

OPIS: avdio/video vtičnik za Kodi. Predvaja žive prenose in arhivske vsebine spletnega portala RTV Slovenija.

NAMESTITEV: prenesi arhivsko datoteko poljubne različice (plugin.video.rtvslo-x.x.x.zip), nato namesti vtičnik s pomočjo Kodi vmesnika.

POMEMBNO:
Tukaj lahko najdete vtičnik različice 3.x.x, ki podpira le naprave z naloženim predvajalnikom Kodi 19 (Matrix)!
Naprave s starejšim sistemom Kodi 18 (Leia) lahko še vedno uporabljajo prejšnjo različico vtičnika (2.x.x), ki je dosegljiva na naslednji povezavi: https://github.com/ro-le/plugin.video.rtvslo.
Stara različica 2.1.3 je zadnja razvita za starejše sisteme in v prihodnosti ne bo več posodobljena.

AVTOR: ro-le

=============